import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyC_UaL_426iKsdyqwd-33bsB_goiNxshvM",
    authDomain: "teste-eb354.firebaseapp.com",
    databaseURL: "https://teste-eb354.firebaseio.com",
    projectId: "teste-eb354",
    storageBucket: "teste-eb354.appspot.com",
    messagingSenderId: "929080028577",
    appId: "1:929080028577:web:d31242f13a06fc685c2fe0",
    measurementId: "G-8KP1QE34Z1"
};

firebase.initializeApp(config)
firebase.firestore().settings({
    timestampsInSnapshots: true
})

export const myFirebase = firebase
export const myFirestore = firebase.firestore()
export const myStorage = firebase.storage()
